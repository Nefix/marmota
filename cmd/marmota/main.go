/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package main

import (
	"gitlab.com/nefix/marmota/pkg/cfg"
	"gitlab.com/nefix/marmota/pkg/db"
	"gitlab.com/nefix/marmota/pkg/ipfs"
	"gitlab.com/nefix/marmota/pkg/musicbrainz"
)

func main() {
	cfg.Init()
	db.Init()
	ipfs.Init()
	musicbrainz.Init()

	defer db.DB.Close()
}
