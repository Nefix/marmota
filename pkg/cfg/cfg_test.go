/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cfg_test

import (
	"io/ioutil"
	"os"
	"testing"
	"time"

	"gitlab.com/nefix/marmota/pkg/cfg"
	"gitlab.com/nefix/marmota/pkg/utils/tests"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

func TestInit(t *testing.T) {
	assert := assert.New(t)

	t.Run("should initialize correctly", func(t *testing.T) {
		err := ioutil.WriteFile("config.toml", []byte(""), 0644)
		assert.Nil(err)

		cfg.Init()
		assert.Equal("https://marmota.example.org", cfg.Config.GetString("marmota.URL"))

		err = os.Remove("config.toml")
		assert.Nil(err)
	})

	t.Run("should exit if there's an error intializing the configuration", func(t *testing.T) {
		tests.AssertExits(t, cfg.Init)
	})

	t.Run("should reload correctly the configuration", func(t *testing.T) {
		err := ioutil.WriteFile("config.toml", []byte(""), 0644)
		assert.Nil(err)

		cfg.Init()
		assert.Equal("https://marmota.example.org", cfg.Config.GetString("marmota.URL"))

		err = ioutil.WriteFile("config.toml", []byte(`[marmota]
URL = "https://marmota.dev"`), 0644)
		assert.Nil(err)

		time.Sleep(1 * time.Second)

		assert.Equal("https://marmota.dev", cfg.Config.GetString("marmota.URL"))

		err = os.Remove("config.toml")
		assert.Nil(err)
	})
}

func TestSetDefaults(t *testing.T) {
	assert := assert.New(t)

	cfg.Config = viper.New()
	cfg.SetDefaults()

	expectedCfg := map[string]interface{}{
		"marmota": map[string]interface{}{
			"url": "https://marmota.example.org",
		},
		"musicbrainz": map[string]interface{}{
			"url": "https://musicbrainz.org/ws/2",
		},
		"db": map[string]interface{}{
			"type":     "sqlite",
			"dir":      "/var/lib/marmota/database",
			"username": "marmota",
			"password": "P4$$w0rd!",
			"host":     "localhost",
			"port":     5432,
			"name":     "marmota",
		},
		"library": map[string]interface{}{
			"dir": "/srv/marmota/library",
		},
		"ipfs": map[string]interface{}{
			"host": "localhost",
			"port": 5001,
		},
	}

	assert.Equal(expectedCfg, cfg.Config.AllSettings())
}
