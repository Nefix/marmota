/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package db_test

import (
	"testing"

	"gitlab.com/nefix/marmota/pkg/db"
	"gitlab.com/nefix/marmota/pkg/models"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func TestAutoMigrate(t *testing.T) {
	assert := assert.New(t)

	var err error
	db.DB, err = gorm.Open("sqlite3", ":memory:")
	assert.Nil(err)

	assert.False(db.DB.HasTable(&models.File{}))
	assert.False(db.DB.HasTable(&models.Instance{}))
	assert.False(db.DB.HasTable(&models.User{}))

	db.AutoMigrate()

	assert.True(db.DB.HasTable(&models.File{}))
	assert.True(db.DB.HasTable(&models.Instance{}))
	assert.True(db.DB.HasTable(&models.User{}))
}
