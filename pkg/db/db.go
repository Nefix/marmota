/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package db

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/nefix/marmota/pkg/cfg"
	"gitlab.com/nefix/marmota/pkg/models"

	"github.com/jinzhu/gorm"
	// Microsoft SQL server
	_ "github.com/jinzhu/gorm/dialects/mssql"
	// MySQL / MariaDB
	_ "github.com/jinzhu/gorm/dialects/mysql"
	// PostgreSQL
	_ "github.com/jinzhu/gorm/dialects/postgres"
	// SQLite 3
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	jww "github.com/spf13/jwalterweatherman"
)

// DB is the open database connection
var DB *gorm.DB

// Init opens the connection to the DB and runs the migrations
func Init() {
	dbType := cfg.Config.GetString("db.type")

	var err error

	switch dbType {
	case "mssql":
		usr := cfg.Config.GetString("db.username")
		pwd := cfg.Config.GetString("db.password")
		host := cfg.Config.GetString("db.host")
		port := cfg.Config.GetInt("db.port")
		dbName := cfg.Config.GetString("db.name")

		DB, err = gorm.Open("mssql", fmt.Sprintf("sqlserver://%s:%s@%s:%d?database=%s", usr, pwd, host, port, dbName))
		if err != nil {
			jww.FATAL.Printf("error opening the database: %v", err)
			os.Exit(1)
		}

	case "mysql":
		usr := cfg.Config.GetString("db.username")
		pwd := cfg.Config.GetString("db.password")
		host := cfg.Config.GetString("db.host")
		port := cfg.Config.GetInt("db.port")
		dbName := cfg.Config.GetString("db.name")

		DB, err = gorm.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", usr, pwd, host, port, dbName))
		if err != nil {
			jww.FATAL.Printf("error opening the database: %v", err)
			os.Exit(1)
		}

	case "postgres":
		usr := cfg.Config.GetString("db.username")
		pwd := cfg.Config.GetString("db.password")
		host := cfg.Config.GetString("db.host")
		port := cfg.Config.GetInt("db.port")
		dbName := cfg.Config.GetString("db.name")

		DB, err = gorm.Open("postges", fmt.Sprintf("user=%s password=%s host=%s port=%d dbname=%s", usr, pwd, host, port, dbName))
		if err != nil {
			jww.FATAL.Printf("error opening the database: %v", err)
			os.Exit(1)
		}

	default:
		dir := cfg.Config.GetString("db.dir")

		DB, err = gorm.Open("sqlite3", filepath.Join(dir, "marmota.db"))
		if err != nil {
			jww.FATAL.Printf("error opening the database: %v", err)
			os.Exit(1)
		}
	}

	AutoMigrate()
}

// AutoMigrate runs all the migrations
func AutoMigrate() {
	DB.AutoMigrate(&models.Instance{})
	DB.AutoMigrate(&models.File{})
	DB.AutoMigrate(&models.User{})
}
