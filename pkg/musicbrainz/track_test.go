/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package musicbrainz_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/nefix/marmota/pkg/cfg"
	"gitlab.com/nefix/marmota/pkg/fs"
	"gitlab.com/nefix/marmota/pkg/musicbrainz"
	"gitlab.com/nefix/marmota/pkg/utils/tests"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGetTrack(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	t.Run("should return the track correctly", func(t *testing.T) {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")

			fmt.Fprintf(w, `{"release-offset":0,"releases":[{"packaging-id":null,"status-id":"4e304316-386d-3409-af2e-78857eec5cfe","media":[{"tracks":[{"number":"A1","id":"7454b897-a596-47ff-b765-0c56ac22c839","artist-credit":[{"name":"Accidente","joinphrase":"","artist":{"id":"4461a9e7-2168-4453-a806-486c568190d5","disambiguation":"","sort-name":"Accidente","name":"Accidente"}}],"recording":{"length":171000,"title":"La utopía ya es real","id":"8b5e3db2-7d48-43e0-82b3-2e2d8e942ed6","disambiguation":"","artist-credit":[{"artist":{"sort-name":"Accidente","id":"4461a9e7-2168-4453-a806-486c568190d5","disambiguation":"","name":"Accidente"},"joinphrase":"","name":"Accidente"}],"video":false},"position":1,"length":171000,"title":"La utopía ya es real"},{"recording":{"length":60000,"title":"Si te marchas ahora","disambiguation":"","id":"2f0cfe38-aac8-4f01-b025-4e796377e8cd","artist-credit":[{"artist":{"name":"Accidente","sort-name":"Accidente","disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5"},"name":"Accidente","joinphrase":""}],"video":false},"artist-credit":[{"artist":{"name":"Accidente","sort-name":"Accidente","disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5"},"name":"Accidente","joinphrase":""}],"id":"807e0ec5-dc2d-427e-9cce-96cb56067882","number":"A2","length":60000,"title":"Si te marchas ahora","position":2},{"number":"A3","artist-credit":[{"joinphrase":"","name":"Accidente","artist":{"name":"Accidente","disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","sort-name":"Accidente"}}],"id":"2a12b326-fe4c-477a-a1c6-c77b645e4447","recording":{"id":"2b502629-5fd0-418f-9212-e6e91d8c672d","video":false,"artist-credit":[{"artist":{"sort-name":"Accidente","id":"4461a9e7-2168-4453-a806-486c568190d5","disambiguation":"","name":"Accidente"},"joinphrase":"","name":"Accidente"}],"disambiguation":"","length":85000,"title":"Querer la libertad"},"position":3,"title":"Querer la libertad","length":85000},{"id":"4669c59e-38ec-4708-b1a4-66ed84c2582c","artist-credit":[{"artist":{"sort-name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","disambiguation":"Spanish hardcore","name":"Duelo"},"name":"Duelo","joinphrase":""}],"recording":{"length":110000,"title":"Comarcal 220","artist-credit":[{"artist":{"sort-name":"Duelo","disambiguation":"Spanish hardcore","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo"},"joinphrase":"","name":"Duelo"}],"id":"ea46c5f3-b098-4eca-b20f-a558fea1306b","disambiguation":"","video":false},"number":"B1","title":"Comarcal 220","length":110000,"position":4},{"length":63000,"title":"Buscar","position":5,"id":"32e076d3-aa90-4061-861a-0c77b0892ce8","artist-credit":[{"name":"Duelo","joinphrase":"","artist":{"sort-name":"Duelo","disambiguation":"Spanish hardcore","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo"}}],"recording":{"id":"af7fe28f-4570-4b25-9ce9-dfa30708dc98","artist-credit":[{"name":"Duelo","joinphrase":"","artist":{"id":"473154da-c99d-41e8-9791-2300f388b0a8","disambiguation":"Spanish hardcore","sort-name":"Duelo","name":"Duelo"}}],"video":false,"disambiguation":"","title":"Buscar","length":63000},"number":"B2"},{"number":"B3","id":"1fa36d6b-ffc1-4ab4-ab96-4c4cd8297791","artist-credit":[{"name":"Duelo","joinphrase":"","artist":{"name":"Duelo","sort-name":"Duelo","disambiguation":"Spanish hardcore","id":"473154da-c99d-41e8-9791-2300f388b0a8"}}],"recording":{"artist-credit":[{"joinphrase":"","name":"Duelo","artist":{"name":"Duelo","sort-name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","disambiguation":"Spanish hardcore"}}],"id":"deee0375-f16f-4460-8fe7-e24a937d5bf5","disambiguation":"","video":false,"title":"Madrid 2011","length":131000},"position":6,"length":131000,"title":"Madrid 2011"}],"format-id":"1d5a48e4-e94a-36dc-84e2-0c5f9f880aa7","track-count":6,"format":"7\" Vinyl","title":"","track-offset":0,"position":1}],"disambiguation":"","cover-art-archive":{"artwork":true,"darkened":false,"count":1,"back":false,"front":true},"date":"2015-04-28","text-representation":{"language":"spa","script":"Latn"},"asin":null,"artist-credit":[{"joinphrase":" / ","name":"Duelo","artist":{"sort-name":"Duelo","disambiguation":"Spanish hardcore","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo"}},{"artist":{"sort-name":"Accidente","id":"4461a9e7-2168-4453-a806-486c568190d5","disambiguation":"","name":"Accidente"},"name":"Accidente","joinphrase":""}],"country":"ES","title":"Duelo / Accidente","barcode":null,"id":"4afebb2e-5d8e-422b-aab8-570bfa06c9df","quality":"normal","release-events":[{"area":{"iso-3166-1-codes":["ES"],"name":"Spain","disambiguation":"","id":"471c46a7-afc5-31c4-923c-d0444f5053a4","sort-name":"Spain"},"date":"2015-04-28"}],"packaging":null,"status":"Official"}],"release-count":1}`)
		}))
		defer ts.Close()

		// Init the Mocks
		fs.FS = afero.NewMemMapFs()
		err := tests.InitCfg()
		require.Nil(err)
		cfg.Config.Set("musicbrainz.URL", ts.URL+"/ws/2")
		musicbrainz.Init()

		expectedTrack := musicbrainz.Track{
			MBID:              "2a12b326-fe4c-477a-a1c6-c77b645e4447",
			Title:             "Querer la libertad",
			ArtistName:        "Accidente",
			ReleaseArtistName: "Duelo / Accidente",
			ReleaseName:       "Duelo / Accidente",
			Length:            85000,
		}

		track, err := musicbrainz.GetTrack("2a12b326-fe4c-477a-a1c6-c77b645e4447")
		assert.Nil(err)
		assert.Equal(expectedTrack, track)
	})

	t.Run("should return an error if there's an error calling the MusicBrainz API", func(t *testing.T) {
		// Init the Mocks
		fs.FS = afero.NewMemMapFs()
		err := tests.InitCfg()
		require.Nil(err)
		cfg.Config.Set("musicbrainz.URL", "")
		musicbrainz.Init()

		expectedTrack := musicbrainz.Track{}

		track, err := musicbrainz.GetTrack("2a12b326-fe4c-477a-a1c6-c77b645e4447")
		assert.EqualError(err, `error calling the API: error getting "release": Get release?fmt=json&inc=artist-credits&track=2a12b326-fe4c-477a-a1c6-c77b645e4447: unsupported protocol scheme ""`)
		assert.Equal(expectedTrack, track)
	})

	t.Run("should return an error if there's an error unmarshaling the JSON response", func(t *testing.T) {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")

			fmt.Fprintf(w, `{`)
		}))
		defer ts.Close()

		// Init the Mocks
		fs.FS = afero.NewMemMapFs()
		err := tests.InitCfg()
		require.Nil(err)
		cfg.Config.Set("musicbrainz.URL", ts.URL+"/ws/2")
		musicbrainz.Init()

		expectedTrack := musicbrainz.Track{}

		track, err := musicbrainz.GetTrack("2a12b326-fe4c-477a-a1c6-c77b645e4447")
		assert.EqualError(err, "error unmarshaling the response: unexpected end of JSON input")
		assert.Equal(expectedTrack, track)
	})
}
