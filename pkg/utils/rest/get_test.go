/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package rest_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/nefix/marmota/pkg/utils/rest"

	"github.com/stretchr/testify/assert"
)

func TestGet(t *testing.T) {
	assert := assert.New(t)

	t.Run("should return the response as bytes", func(t *testing.T) {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")

			fmt.Fprintf(w, `{
	"artist": "Duelo",
	"title": "Relaciones Horizontales"
}`)
		}))
		defer ts.Close()

		cli, err := rest.NewClient(ts.URL)
		assert.Nil(err)

		expectedRsp := []byte(`{
	"artist": "Duelo",
	"title": "Relaciones Horizontales"
}`)

		rsp, err := cli.Get("test", map[string]string{})
		assert.Nil(err)
		assert.Equal(expectedRsp, rsp)
	})

	t.Run("there's an error querying the endpoint", func(t *testing.T) {
		cli, err := rest.NewClient("")
		assert.Nil(err)

		var expectedRsp []byte = nil
		expectedErr := `error getting "test": Get test: unsupported protocol scheme ""`

		rsp, err := cli.Get("test", map[string]string{})
		assert.EqualError(err, expectedErr)
		assert.Equal(expectedRsp, rsp)
	})

	t.Run("should return an error if the code isn't 200", func(t *testing.T) {
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			http.Error(w, "error!", http.StatusInternalServerError)
		}))
		defer ts.Close()

		cli, err := rest.NewClient(ts.URL)
		assert.Nil(err)

		var expectedRsp []byte = nil
		expectedErr := "HTTP Code is 500"

		rsp, err := cli.Get("test", map[string]string{})
		assert.EqualError(err, expectedErr)
		assert.Equal(expectedRsp, rsp)
	})
}
