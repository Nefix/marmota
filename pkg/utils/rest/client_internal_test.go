/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package rest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFullURL(t *testing.T) {
	assert := assert.New(t)

	t.Run("should return the correct URL", func(t *testing.T) {
		cli, err := NewClient("https://nefixestrada.com/api/v0")
		assert.Nil(err)

		expectedURL := "https://nefixestrada.com/api/v0/list-songs?fmt=json"
		fullURL := cli.fullURL("list-songs", map[string]string{
			"fmt": "json",
		})
		assert.Equal(expectedURL, fullURL)

		// the method isn't chaning the original url
		expectedURL = "https://nefixestrada.com/api/v0/list-users"
		fullURL = cli.fullURL("list-users", map[string]string{})
		assert.Equal(expectedURL, fullURL)
	})
}
