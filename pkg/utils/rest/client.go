/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package rest

import (
	"fmt"
	"net/http"
	"net/url"
	"path"
)

// Client is the REST API client
type Client struct {
	baseURL *url.URL
	client  *http.Client
}

// NewClient generates a new HTTP REST client
func NewClient(cliURL string) (*Client, error) {
	u, err := url.Parse(cliURL)
	if err != nil {
		return &Client{}, fmt.Errorf("error parsing client URL: %v", err)
	}

	return &Client{
		baseURL: u,
		client:  &http.Client{},
	}, nil
}

// fullURL returns the full URL of the endpoint that is going to be called with the arguments encoded
func (c *Client) fullURL(endpoint string, args map[string]string) string {
	// copy the URL and modify the path
	tmpURL := &url.URL{}
	*tmpURL = *c.baseURL
	tmpURL.Path = path.Join(c.baseURL.Path, endpoint)

	values := url.Values{}
	for k, v := range args {
		values.Add(k, v)
	}

	tmpURL.RawQuery = values.Encode()

	return tmpURL.String()
}
