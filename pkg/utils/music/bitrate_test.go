/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package music

import (
	"testing"

	"gitlab.com/nefix/marmota/pkg/fs"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
)

func TestGetBitrate(t *testing.T) {
	assert := assert.New(t)

	t.Run("should return the correct bitrate", func(t *testing.T) {
		fs.FS = afero.NewMemMapFs()
		afero.WriteFile(fs.FS, "/example", []byte("imagine this is the song content"), 0644)

		bitrate, err := GetBitrate("/example", 4)
		assert.Nil(err)
		assert.Equal(64, bitrate)
	})

	t.Run("should return an error if the file doesn't exist", func(t *testing.T) {
		fs.FS = afero.NewMemMapFs()

		bitrate, err := GetBitrate("/example", 4)
		assert.EqualError(err, "error getting the file bitrate: error reading the file /example stats: open /example: file does not exist")
		assert.Equal(0, bitrate)
	})
}
