/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package user_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/nefix/marmota/pkg/actions/user"
	"gitlab.com/nefix/marmota/pkg/db"
	"gitlab.com/nefix/marmota/pkg/models"
	"gitlab.com/nefix/marmota/pkg/utils/tests"
)

func TestCreate(t *testing.T) {
	require := require.New(t)
	assert := assert.New(t)

	t.Run("should create the user correctly", func(t *testing.T) {
		require.Nil(tests.InitDB())

		expectedUsr := &models.User{
			ID:   "nefix",
			Name: "Néfix Estrada",
		}

		assert.Nil(user.Create("nefix", "Néfix Estrada"))

		usr := &models.User{}
		assert.Nil(db.DB.Where(&models.User{ID: "nefix"}).First(usr).Error)

		assert.Equal(expectedUsr.ID, usr.ID)
		assert.Equal(expectedUsr.Name, usr.Name)
		assert.NotNil(usr.CreatedAt)
		assert.NotNil(usr.UpdatedAt)
	})

	t.Run("should return an error if the username isn't valid", func(t *testing.T) {
		require.Nil(tests.InitDB())

		assert.EqualError(user.Create("/", "I'm a slash!"), "user ID / is invalid")
	})

	t.Run("should return an error if there's an error adding the user to the DB", func(t *testing.T) {
		require.Nil(tests.InitDB())

		expectedUsr := &models.User{
			ID:   "nefix",
			Name: "Néfix Estrada",
		}

		assert.Nil(db.DB.Create(expectedUsr).Error)
		assert.EqualError(user.Create("nefix", "Néfix Estrada"), "error adding nefix to the DB: UNIQUE constraint failed: users.id")
	})
}
