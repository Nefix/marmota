/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package user

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInvalidUserID(t *testing.T) {
	assert := assert.New(t)

	invalidIDs := []string{
		"!",
		"#",
		"",
		"()",
		"-",
		"-hello",
		".",
		".hello",
		"_",
		"a@!/",
		"hel--lo",
		"hel-.lo",
		"hel..lo",
		"hel__lo",
		"hello-",
		"hello.",
		"\\",
		"\\\\",
		"--1",
		"!@#$%^&*()`~",
		"`⁄€‹›ﬁﬂ‡°·‚—±",
		"⅛⅜⅝⅞",
		"😍",
		"👩🏽",
		"👾 🙇 💁 🙅 🙆 🙋 🙎 🙍 ",
		"🐵 🙈 🙉 🙊",
		"❤️ 💔 💌 💕 💞 💓 💗 💖 💘 💝 💟 💜 💛 💚 💙",
		"✋🏿 💪🏿 👐🏿 🙌🏿 👏🏿 🙏🏿",
		"🚾 🆒 🆓 🆕 🆖 🆗 🆙 🏧",
		"0️⃣ 1️⃣ 2️⃣ 3️⃣ 4️⃣ 5️⃣ 6️⃣ 7️⃣ 8️⃣ 9️⃣ 🔟",
		"１２３",
		" ",
		"𝐓𝐡𝐞",
		"⒯⒣⒠",
	}
	validIDs := []string{
		"a",
		"10101",
		"1he-llo",
		"_hello",
		"he-llo",
		"he.llo_",
		"hello",
		"hello_",
		"999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999",
	}

	for _, id := range invalidIDs {
		assert.False(validateUserID(id))
	}

	for _, id := range validIDs {
		assert.True(validateUserID(id))
	}
}
