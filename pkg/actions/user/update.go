/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package user

import (
	"fmt"

	"gitlab.com/nefix/marmota/pkg/db"
	"gitlab.com/nefix/marmota/pkg/models"
)

// Update updates a user to the DB. The user has to be previoulsy modified. If the user doesn't exist, is going to be created
// e.g. get the user -> modify the struct -> run user.Update()
func Update(usr *models.User) error {
	if err := db.DB.Save(usr).Error; err != nil {
		return fmt.Errorf("error updating %s in the DB: %v", usr.ID, err)
	}

	return nil
}
