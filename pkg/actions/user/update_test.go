/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package user_test

import (
	"testing"

	"gitlab.com/nefix/marmota/pkg/actions/user"
	"gitlab.com/nefix/marmota/pkg/db"
	"gitlab.com/nefix/marmota/pkg/models"
	"gitlab.com/nefix/marmota/pkg/utils/tests"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestUpdate(t *testing.T) {
	require := require.New(t)
	assert := assert.New(t)

	t.Run("should update the user correctly", func(t *testing.T) {
		require.Nil(tests.InitDB())

		expectedUsr := &models.User{
			ID:   "no_gods_no_managers",
			Name: "Mikhail Bakunin",
		}

		assert.Nil(db.DB.Create(expectedUsr).Error)

		expectedUsr.Name = "aa"

		assert.Nil(user.Update(expectedUsr))

		usr := &models.User{}
		assert.Nil(db.DB.Where(&models.User{ID: "no_gods_no_managers"}).First(usr).Error)

		assert.Equal(expectedUsr.ID, usr.ID)
		assert.Equal(expectedUsr.Name, usr.Name)
		assert.NotNil(usr.CreatedAt)
		assert.NotNil(usr.UpdatedAt)
	})

	t.Run("should return an error if there's an error updating the user", func(t *testing.T) {
		db.DB.DropTable("users")
		expectedUsr := &models.User{
			ID:   "no_gods_no_managers",
			Name: "Emma Goldman",
		}

		assert.EqualError(user.Update(expectedUsr), "error updating no_gods_no_managers in the DB: no such table: users")
	})
}
