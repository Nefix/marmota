/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package user

import (
	"fmt"

	"gitlab.com/nefix/marmota/pkg/db"
	"gitlab.com/nefix/marmota/pkg/models"
)

// Create adds a new user to the DB
func Create(id, name string) error {
	if !validateUserID(id) {
		return fmt.Errorf("user ID %s is invalid", id)
	}

	usr := &models.User{
		ID:   id,
		Name: name,
	}

	if err := db.DB.Create(usr).Error; err != nil {
		return fmt.Errorf("error adding %s to the DB: %v", id, err)
	}

	return nil
}
