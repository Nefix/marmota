/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package user

import "regexp"

// validateUserID checks if an user id is valid (user friendly)
// Taken from https://github.com/theskumar/python-usernames
func validateUserID(id string) bool {
	// not only _
	exp := regexp.MustCompile(`^_$`)
	if exp.MatchString(id) {
		return false
	}

	// no - or . at the beginning
	exp = regexp.MustCompile(`^[.-]`)
	if exp.MatchString(id) {
		return false
	}

	// no __ or .. or -- or any combination of them inside
	exp = regexp.MustCompile(`^.*[_.-]{2}`)
	if exp.MatchString(id) {
		return false
	}

	// allowed characters, at least one must be present
	exp = regexp.MustCompile(`^[a-zA-Z0-9_.-]+$`)
	if !exp.MatchString(id) {
		return false
	}

	//no - or . at the end
	exp = regexp.MustCompile(`.*[.-]$`)
	if exp.MatchString(id) {
		return false
	}

	return true
}
