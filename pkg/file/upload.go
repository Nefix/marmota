/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package file

import (
	"fmt"
	"path/filepath"
	"time"

	"gitlab.com/nefix/marmota/pkg/cfg"
	"gitlab.com/nefix/marmota/pkg/db"
	"gitlab.com/nefix/marmota/pkg/fs"
	"gitlab.com/nefix/marmota/pkg/ipfs"
	"gitlab.com/nefix/marmota/pkg/models"
	"gitlab.com/nefix/marmota/pkg/musicbrainz"
	"gitlab.com/nefix/marmota/pkg/utils/file"
	"gitlab.com/nefix/marmota/pkg/utils/music"

	"github.com/michiwend/gomusicbrainz"
	"github.com/spf13/afero"
	jww "github.com/spf13/jwalterweatherman"
)

// Upload adds a file to Marmota (writes it to the server, adds it to IPFS, pins it and adds the record to the DB)
func Upload(trackID gomusicbrainz.MBID, d []byte) error {
	t, err := musicbrainz.GetTrack(trackID)
	if err != nil {
		jww.ERROR.Printf("error uploading a file: error getting the track %s: %v", trackID, err)
		return fmt.Errorf("error getting the track %s: %v", trackID, err)
	}

	dir := filepath.Join(cfg.Config.GetString("library.dir"), file.FixName(t.ReleaseArtistName), file.FixName(t.ReleaseName))
	exists, err := afero.DirExists(fs.FS, dir)
	if err != nil {
		jww.ERROR.Printf("error uploading a file: error checking directory existence %s: %v", dir, err)
		return fmt.Errorf("error checking directory existence %s: %v", dir, err)
	}

	if !exists {
		err = fs.FS.MkdirAll(dir, 0755)
		if err != nil {
			jww.ERROR.Printf("error uploading a file: error creating the required directory %s: %v", dir, err)
			return fmt.Errorf("error creating the required directory %s: %v", dir, err)
		}
	}

	fPath := filepath.Join(dir, fmt.Sprintf(
		"%s - %s - %s",
		file.FixName(t.ArtistName),
		file.FixName(t.Title),
		time.Now().Format("2006-01-02T15:04:05"),
	))

	err = afero.WriteFile(fs.FS, fPath, d, 0644)
	if err != nil {
		jww.ERROR.Printf("error uploading a file: error writing the file %s: %v", fPath, err)
		return fmt.Errorf("error writing the file %s: %v", fPath, err)
	}

	cid, err := ipfs.Cli.Add(fPath, true)
	if err != nil {
		jww.ERROR.Printf("error uploading a file: error adding the file %s to IPFS: %v", fPath, err)
		return fmt.Errorf("error adding the file %s to IPFS: %v", fPath, err)
	}

	bitrate, err := music.GetBitrate(fPath, t.Length)
	if err != nil {
		jww.ERROR.Printf("error uploading a file: error getting the bitrate of %s: %v", fPath, err)
		return fmt.Errorf("error getting the bitrate of %s: %v", fPath, err)
	}

	file := &models.File{
		ID:        cid,
		MBID:      trackID,
		ServerURL: cfg.Config.GetString("marmota.URL"),
		Bitrate:   bitrate,
	}

	if err := db.DB.Create(file).Error; err != nil {
		jww.ERROR.Printf("error uploading a file: error adding %s to the DB: %v", trackID, err)
		return fmt.Errorf("error adding %s to the DB: %v", trackID, err)
	}

	jww.DEBUG.Printf("%s (%s) uploaded successfully", trackID, fPath)

	return nil
}
