/*
 * Copyright (C) 2019 The Marmota Project
 * Authors: The Marmota Authors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package file

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"gitlab.com/nefix/marmota/pkg/cfg"
	"gitlab.com/nefix/marmota/pkg/db"
	"gitlab.com/nefix/marmota/pkg/fs"
	"gitlab.com/nefix/marmota/pkg/ipfs"
	"gitlab.com/nefix/marmota/pkg/models"
	"gitlab.com/nefix/marmota/pkg/musicbrainz"
	"gitlab.com/nefix/marmota/pkg/utils/tests"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

type ipfsMock struct {
	mock.Mock
}

// Add a file to IPFS
func (i *ipfsMock) Add(path string, pin bool) (string, error) {
	args := i.Called(path, pin)

	return args.String(0), args.Error(1)
}

func TestUpload(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	const trackID = "2a12b326-fe4c-477a-a1c6-c77b645e4447"
	const cid = "thisisacid"

	theIPFSMock := &ipfsMock{}
	theIPFSMock.On("Add", mock.MatchedBy(func(path string) bool {
		return strings.HasPrefix(path, "/srv/marmota/library/Duelo _ Accidente/Duelo _ Accidente/Accidente - Querer la libertad")
	}), true).Return(cid, nil)
	theIPFSMock.On("Add", mock.MatchedBy(func(path string) bool {
		return strings.HasPrefix(path, "/srv/marmota/library/Rude Pride _ Seaside Rebels/On Common Ground/Rude Pride - 1886")
	}), true).Return("", errors.New("this song is too good"))

	t.Run("should upload it correctly", func(t *testing.T) {
		fs.FS = afero.NewMemMapFs()

		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")

			fmt.Fprintf(w, `{"release-offset":0,"release-count":1,"releases":[{"barcode":null,"quality":"normal","date":"2015-04-28","title":"Duelo / Accidente","text-representation":{"script":"Latn","language":"spa"},"disambiguation":"","cover-art-archive":{"back":false,"artwork":true,"darkened":false,"front":true,"count":1},"artist-credit":[{"joinphrase":" / ","artist":{"sort-name":"Duelo","disambiguation":"Spanish hardcore","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo"},"name":"Duelo"},{"joinphrase":"","artist":{"sort-name":"Accidente","disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente"},"name":"Accidente"}],"status":"Official","packaging-id":null,"packaging":null,"status-id":"4e304316-386d-3409-af2e-78857eec5cfe","id":"4afebb2e-5d8e-422b-aab8-570bfa06c9df","country":"ES","release-events":[{"area":{"sort-name":"Spain","id":"471c46a7-afc5-31c4-923c-d0444f5053a4","name":"Spain","iso-3166-1-codes":["ES"],"disambiguation":""},"date":"2015-04-28"}],"asin":null,"media":[{"format-id":"1d5a48e4-e94a-36dc-84e2-0c5f9f880aa7","tracks":[{"number":"A1","position":1,"id":"7454b897-a596-47ff-b765-0c56ac22c839","title":"La utopía ya es real","length":171000,"artist-credit":[{"artist":{"sort-name":"Accidente","disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente"},"name":"Accidente","joinphrase":""}],"recording":{"disambiguation":"","video":false,"title":"La utopía ya es real","id":"8b5e3db2-7d48-43e0-82b3-2e2d8e942ed6","artist-credit":[{"joinphrase":"","name":"Accidente","artist":{"id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente","disambiguation":"","sort-name":"Accidente"}}],"length":171000}},{"recording":{"length":60000,"artist-credit":[{"joinphrase":"","name":"Accidente","artist":{"sort-name":"Accidente","disambiguation":"","name":"Accidente","id":"4461a9e7-2168-4453-a806-486c568190d5"}}],"disambiguation":"","video":false,"title":"Si te marchas ahora","id":"2f0cfe38-aac8-4f01-b025-4e796377e8cd"},"artist-credit":[{"joinphrase":"","artist":{"disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente","sort-name":"Accidente"},"name":"Accidente"}],"length":60000,"title":"Si te marchas ahora","number":"A2","position":2,"id":"807e0ec5-dc2d-427e-9cce-96cb56067882"},{"length":85000,"artist-credit":[{"artist":{"disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente","sort-name":"Accidente"},"name":"Accidente","joinphrase":""}],"recording":{"length":85000,"artist-credit":[{"name":"Accidente","artist":{"sort-name":"Accidente","name":"Accidente","id":"4461a9e7-2168-4453-a806-486c568190d5","disambiguation":""},"joinphrase":""}],"disambiguation":"","video":false,"title":"Querer la libertad","id":"2b502629-5fd0-418f-9212-e6e91d8c672d"},"position":3,"number":"A3","id":"2a12b326-fe4c-477a-a1c6-c77b645e4447","title":"Querer la libertad"},{"position":4,"id":"4669c59e-38ec-4708-b1a4-66ed84c2582c","number":"B1","title":"Comarcal 220","artist-credit":[{"artist":{"name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","disambiguation":"Spanish hardcore","sort-name":"Duelo"},"name":"Duelo","joinphrase":""}],"length":110000,"recording":{"artist-credit":[{"joinphrase":"","artist":{"sort-name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo","disambiguation":"Spanish hardcore"},"name":"Duelo"}],"length":110000,"title":"Comarcal 220","id":"ea46c5f3-b098-4eca-b20f-a558fea1306b","disambiguation":"","video":false}},{"artist-credit":[{"joinphrase":"","artist":{"sort-name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo","disambiguation":"Spanish hardcore"},"name":"Duelo"}],"length":63000,"recording":{"artist-credit":[{"name":"Duelo","artist":{"name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","disambiguation":"Spanish hardcore","sort-name":"Duelo"},"joinphrase":""}],"length":63000,"id":"af7fe28f-4570-4b25-9ce9-dfa30708dc98","title":"Buscar","video":false,"disambiguation":""},"number":"B2","position":5,"id":"32e076d3-aa90-4061-861a-0c77b0892ce8","title":"Buscar"},{"length":131000,"artist-credit":[{"name":"Duelo","artist":{"disambiguation":"Spanish hardcore","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo","sort-name":"Duelo"},"joinphrase":""}],"recording":{"artist-credit":[{"name":"Duelo","artist":{"disambiguation":"Spanish hardcore","name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","sort-name":"Duelo"},"joinphrase":""}],"length":131000,"id":"deee0375-f16f-4460-8fe7-e24a937d5bf5","title":"Madrid 2011","video":false,"disambiguation":""},"number":"B3","position":6,"id":"1fa36d6b-ffc1-4ab4-ab96-4c4cd8297791","title":"Madrid 2011"}],"format":"7\" Vinyl","position":1,"track-count":6,"title":"","track-offset":0}]}]}`)
		}))
		defer ts.Close()

		// Init the Mocks
		err := tests.InitCfg()
		require.Nil(err)

		err = tests.InitDB()
		require.Nil(err)

		ipfs.Cli = theIPFSMock

		cfg.Config.Set("MusicBrainz.URL", ts.URL)
		musicbrainz.Init()

		file := filepath.Join(cfg.Config.GetString("library.dir"), "Duelo _ Accidente/Duelo _ Accidente/Accidente - Querer la libertad - "+time.Now().Format("2006-01-02T15:04:05"))
		err = Upload(trackID, []byte("imagine this is the song content"))
		assert.Nil(err)

		// File exists
		exists, err := afero.Exists(fs.FS, file)
		assert.Nil(err)
		assert.True(exists)

		// File has the correct content
		b, err := afero.ReadFile(fs.FS, file)
		assert.Nil(err)
		assert.Equal([]byte("imagine this is the song content"), b)

		// File was added and pinned to IPFS
		theIPFSMock.AssertNumberOfCalls(t, "Add", 1)

		// File was added to the DB
		err = db.DB.Where("id = ?", cid).First(&models.File{}).Error
		assert.Nil(err)
	})

	t.Run("should return an error if there's an error getting the track", func(t *testing.T) {
		fs.FS = afero.NewMemMapFs()

		// Init the Mocks
		err := tests.InitCfg()
		require.Nil(err)

		cfg.Config.Set("MusicBrainz.URL", "")
		musicbrainz.Init()

		err = Upload(trackID, []byte("imagine this is the song content"))
		assert.EqualError(err, fmt.Sprintf(`error getting the track %s: error calling the API: error getting "release": Get release?fmt=json&inc=artist-credits&track=%s: unsupported protocol scheme ""`, trackID, trackID))
	})

	t.Run("should return an error if there's an error creating the directory where the song is going to be stored", func(t *testing.T) {
		fs.FS = afero.NewMemMapFs()

		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")

			fmt.Fprintf(w, `{"release-offset":0,"release-count":1,"releases":[{"barcode":null,"quality":"normal","date":"2015-04-28","title":"Duelo / Accidente","text-representation":{"script":"Latn","language":"spa"},"disambiguation":"","cover-art-archive":{"back":false,"artwork":true,"darkened":false,"front":true,"count":1},"artist-credit":[{"joinphrase":" / ","artist":{"sort-name":"Duelo","disambiguation":"Spanish hardcore","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo"},"name":"Duelo"},{"joinphrase":"","artist":{"sort-name":"Accidente","disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente"},"name":"Accidente"}],"status":"Official","packaging-id":null,"packaging":null,"status-id":"4e304316-386d-3409-af2e-78857eec5cfe","id":"4afebb2e-5d8e-422b-aab8-570bfa06c9df","country":"ES","release-events":[{"area":{"sort-name":"Spain","id":"471c46a7-afc5-31c4-923c-d0444f5053a4","name":"Spain","iso-3166-1-codes":["ES"],"disambiguation":""},"date":"2015-04-28"}],"asin":null,"media":[{"format-id":"1d5a48e4-e94a-36dc-84e2-0c5f9f880aa7","tracks":[{"number":"A1","position":1,"id":"7454b897-a596-47ff-b765-0c56ac22c839","title":"La utopía ya es real","length":171000,"artist-credit":[{"artist":{"sort-name":"Accidente","disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente"},"name":"Accidente","joinphrase":""}],"recording":{"disambiguation":"","video":false,"title":"La utopía ya es real","id":"8b5e3db2-7d48-43e0-82b3-2e2d8e942ed6","artist-credit":[{"joinphrase":"","name":"Accidente","artist":{"id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente","disambiguation":"","sort-name":"Accidente"}}],"length":171000}},{"recording":{"length":60000,"artist-credit":[{"joinphrase":"","name":"Accidente","artist":{"sort-name":"Accidente","disambiguation":"","name":"Accidente","id":"4461a9e7-2168-4453-a806-486c568190d5"}}],"disambiguation":"","video":false,"title":"Si te marchas ahora","id":"2f0cfe38-aac8-4f01-b025-4e796377e8cd"},"artist-credit":[{"joinphrase":"","artist":{"disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente","sort-name":"Accidente"},"name":"Accidente"}],"length":60000,"title":"Si te marchas ahora","number":"A2","position":2,"id":"807e0ec5-dc2d-427e-9cce-96cb56067882"},{"length":85000,"artist-credit":[{"artist":{"disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente","sort-name":"Accidente"},"name":"Accidente","joinphrase":""}],"recording":{"length":85000,"artist-credit":[{"name":"Accidente","artist":{"sort-name":"Accidente","name":"Accidente","id":"4461a9e7-2168-4453-a806-486c568190d5","disambiguation":""},"joinphrase":""}],"disambiguation":"","video":false,"title":"Querer la libertad","id":"2b502629-5fd0-418f-9212-e6e91d8c672d"},"position":3,"number":"A3","id":"2a12b326-fe4c-477a-a1c6-c77b645e4447","title":"Querer la libertad"},{"position":4,"id":"4669c59e-38ec-4708-b1a4-66ed84c2582c","number":"B1","title":"Comarcal 220","artist-credit":[{"artist":{"name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","disambiguation":"Spanish hardcore","sort-name":"Duelo"},"name":"Duelo","joinphrase":""}],"length":110000,"recording":{"artist-credit":[{"joinphrase":"","artist":{"sort-name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo","disambiguation":"Spanish hardcore"},"name":"Duelo"}],"length":110000,"title":"Comarcal 220","id":"ea46c5f3-b098-4eca-b20f-a558fea1306b","disambiguation":"","video":false}},{"artist-credit":[{"joinphrase":"","artist":{"sort-name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo","disambiguation":"Spanish hardcore"},"name":"Duelo"}],"length":63000,"recording":{"artist-credit":[{"name":"Duelo","artist":{"name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","disambiguation":"Spanish hardcore","sort-name":"Duelo"},"joinphrase":""}],"length":63000,"id":"af7fe28f-4570-4b25-9ce9-dfa30708dc98","title":"Buscar","video":false,"disambiguation":""},"number":"B2","position":5,"id":"32e076d3-aa90-4061-861a-0c77b0892ce8","title":"Buscar"},{"length":131000,"artist-credit":[{"name":"Duelo","artist":{"disambiguation":"Spanish hardcore","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo","sort-name":"Duelo"},"joinphrase":""}],"recording":{"artist-credit":[{"name":"Duelo","artist":{"disambiguation":"Spanish hardcore","name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","sort-name":"Duelo"},"joinphrase":""}],"length":131000,"id":"deee0375-f16f-4460-8fe7-e24a937d5bf5","title":"Madrid 2011","video":false,"disambiguation":""},"number":"B3","position":6,"id":"1fa36d6b-ffc1-4ab4-ab96-4c4cd8297791","title":"Madrid 2011"}],"format":"7\" Vinyl","position":1,"track-count":6,"title":"","track-offset":0}]}]}`)
		}))
		defer ts.Close()

		// Init the Mocks
		err := tests.InitCfg()
		require.Nil(err)

		fs.FS = afero.NewReadOnlyFs(fs.FS)

		cfg.Config.Set("MusicBrainz.URL", ts.URL)
		musicbrainz.Init()

		err = Upload(trackID, []byte("imagine this is the song content"))
		assert.EqualError(err, "error creating the required directory /srv/marmota/library/Duelo _ Accidente/Duelo _ Accidente: operation not permitted")
	})

	t.Run("should return an error if there's an error writting the song to the FS", func(t *testing.T) {
		fs.FS = afero.NewMemMapFs()
		err := fs.FS.MkdirAll("/srv/marmota/library/Duelo _ Accidente/Duelo _ Accidente", 0755)
		assert.Nil(err)

		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")

			fmt.Fprintf(w, `{"release-offset":0,"release-count":1,"releases":[{"barcode":null,"quality":"normal","date":"2015-04-28","title":"Duelo / Accidente","text-representation":{"script":"Latn","language":"spa"},"disambiguation":"","cover-art-archive":{"back":false,"artwork":true,"darkened":false,"front":true,"count":1},"artist-credit":[{"joinphrase":" / ","artist":{"sort-name":"Duelo","disambiguation":"Spanish hardcore","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo"},"name":"Duelo"},{"joinphrase":"","artist":{"sort-name":"Accidente","disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente"},"name":"Accidente"}],"status":"Official","packaging-id":null,"packaging":null,"status-id":"4e304316-386d-3409-af2e-78857eec5cfe","id":"4afebb2e-5d8e-422b-aab8-570bfa06c9df","country":"ES","release-events":[{"area":{"sort-name":"Spain","id":"471c46a7-afc5-31c4-923c-d0444f5053a4","name":"Spain","iso-3166-1-codes":["ES"],"disambiguation":""},"date":"2015-04-28"}],"asin":null,"media":[{"format-id":"1d5a48e4-e94a-36dc-84e2-0c5f9f880aa7","tracks":[{"number":"A1","position":1,"id":"7454b897-a596-47ff-b765-0c56ac22c839","title":"La utopía ya es real","length":171000,"artist-credit":[{"artist":{"sort-name":"Accidente","disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente"},"name":"Accidente","joinphrase":""}],"recording":{"disambiguation":"","video":false,"title":"La utopía ya es real","id":"8b5e3db2-7d48-43e0-82b3-2e2d8e942ed6","artist-credit":[{"joinphrase":"","name":"Accidente","artist":{"id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente","disambiguation":"","sort-name":"Accidente"}}],"length":171000}},{"recording":{"length":60000,"artist-credit":[{"joinphrase":"","name":"Accidente","artist":{"sort-name":"Accidente","disambiguation":"","name":"Accidente","id":"4461a9e7-2168-4453-a806-486c568190d5"}}],"disambiguation":"","video":false,"title":"Si te marchas ahora","id":"2f0cfe38-aac8-4f01-b025-4e796377e8cd"},"artist-credit":[{"joinphrase":"","artist":{"disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente","sort-name":"Accidente"},"name":"Accidente"}],"length":60000,"title":"Si te marchas ahora","number":"A2","position":2,"id":"807e0ec5-dc2d-427e-9cce-96cb56067882"},{"length":85000,"artist-credit":[{"artist":{"disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente","sort-name":"Accidente"},"name":"Accidente","joinphrase":""}],"recording":{"length":85000,"artist-credit":[{"name":"Accidente","artist":{"sort-name":"Accidente","name":"Accidente","id":"4461a9e7-2168-4453-a806-486c568190d5","disambiguation":""},"joinphrase":""}],"disambiguation":"","video":false,"title":"Querer la libertad","id":"2b502629-5fd0-418f-9212-e6e91d8c672d"},"position":3,"number":"A3","id":"2a12b326-fe4c-477a-a1c6-c77b645e4447","title":"Querer la libertad"},{"position":4,"id":"4669c59e-38ec-4708-b1a4-66ed84c2582c","number":"B1","title":"Comarcal 220","artist-credit":[{"artist":{"name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","disambiguation":"Spanish hardcore","sort-name":"Duelo"},"name":"Duelo","joinphrase":""}],"length":110000,"recording":{"artist-credit":[{"joinphrase":"","artist":{"sort-name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo","disambiguation":"Spanish hardcore"},"name":"Duelo"}],"length":110000,"title":"Comarcal 220","id":"ea46c5f3-b098-4eca-b20f-a558fea1306b","disambiguation":"","video":false}},{"artist-credit":[{"joinphrase":"","artist":{"sort-name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo","disambiguation":"Spanish hardcore"},"name":"Duelo"}],"length":63000,"recording":{"artist-credit":[{"name":"Duelo","artist":{"name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","disambiguation":"Spanish hardcore","sort-name":"Duelo"},"joinphrase":""}],"length":63000,"id":"af7fe28f-4570-4b25-9ce9-dfa30708dc98","title":"Buscar","video":false,"disambiguation":""},"number":"B2","position":5,"id":"32e076d3-aa90-4061-861a-0c77b0892ce8","title":"Buscar"},{"length":131000,"artist-credit":[{"name":"Duelo","artist":{"disambiguation":"Spanish hardcore","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo","sort-name":"Duelo"},"joinphrase":""}],"recording":{"artist-credit":[{"name":"Duelo","artist":{"disambiguation":"Spanish hardcore","name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","sort-name":"Duelo"},"joinphrase":""}],"length":131000,"id":"deee0375-f16f-4460-8fe7-e24a937d5bf5","title":"Madrid 2011","video":false,"disambiguation":""},"number":"B3","position":6,"id":"1fa36d6b-ffc1-4ab4-ab96-4c4cd8297791","title":"Madrid 2011"}],"format":"7\" Vinyl","position":1,"track-count":6,"title":"","track-offset":0}]}]}`)
		}))
		defer ts.Close()

		// Init the Mocks
		err = tests.InitCfg()
		require.Nil(err)

		fs.FS = afero.NewReadOnlyFs(fs.FS)

		cfg.Config.Set("MusicBrainz.URL", ts.URL)
		musicbrainz.Init()

		fTime := time.Now().Format("2006-01-02T15:04:05")
		err = Upload(trackID, []byte("imagine this is the song content"))
		assert.EqualError(err, fmt.Sprintf("error writing the file /srv/marmota/library/Duelo _ Accidente/Duelo _ Accidente/Accidente - Querer la libertad - %s: operation not permitted", fTime))
	})

	t.Run("should return an error if there's an error adding the file to IPFS", func(t *testing.T) {
		fs.FS = afero.NewMemMapFs()

		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")

			fmt.Fprintf(w, `{"release-count":1,"release-offset":0,"releases":[{"text-representation":{"script":null,"language":null},"cover-art-archive":{"artwork":true,"back":false,"front":true,"darkened":false,"count":1},"disambiguation":"","title":"On Common Ground","packaging":null,"artist-credit":[{"joinphrase":" / ","name":"Rude Pride","artist":{"disambiguation":"","id":"758ac620-5631-4b99-bd1f-778ca4319748","name":"Rude Pride","sort-name":"Rude Pride"}},{"name":"Seaside Rebels","artist":{"sort-name":"Seaside Rebels","id":"0ae11459-9245-44d0-b3c4-86424caa4fb6","name":"Seaside Rebels","disambiguation":"UK Oi / Punk"},"joinphrase":""}],"status":null,"packaging-id":null,"barcode":null,"date":"2017-05","quality":"normal","media":[{"format":"7\" Vinyl","tracks":[{"title":"45 Years","number":"A1","position":1,"id":"ee86acfa-1eec-421c-971a-29c7343310b2","recording":{"disambiguation":"","video":false,"title":"45 Years","id":"4bb1d01a-608f-4e7c-94c5-8e5d481c3006","length":null,"artist-credit":[{"artist":{"sort-name":"Rude Pride","name":"Rude Pride","id":"758ac620-5631-4b99-bd1f-778ca4319748","disambiguation":""},"name":"Rude Pride","joinphrase":""}]},"artist-credit":[{"artist":{"name":"Rude Pride","id":"758ac620-5631-4b99-bd1f-778ca4319748","disambiguation":"","sort-name":"Rude Pride"},"name":"Rude Pride","joinphrase":""}],"length":null},{"artist-credit":[{"joinphrase":"","name":"Rude Pride","artist":{"disambiguation":"","id":"758ac620-5631-4b99-bd1f-778ca4319748","name":"Rude Pride","sort-name":"Rude Pride"}}],"length":null,"recording":{"id":"9529a1b1-929b-4e18-8002-0f8fce29316b","title":"1886","video":false,"disambiguation":"","artist-credit":[{"name":"Rude Pride","artist":{"sort-name":"Rude Pride","disambiguation":"","id":"758ac620-5631-4b99-bd1f-778ca4319748","name":"Rude Pride"},"joinphrase":""}],"length":null},"id":"7d90fe0a-a384-4289-9666-0bdee5d5957e","position":2,"number":"A2","title":"1886"},{"title":"Ain't The Season To Be Jolly","position":3,"number":"B1","id":"ddd51b49-7083-44ab-b075-360a9b18422a","recording":{"disambiguation":"","video":false,"title":"Ain't The Season To Be Jolly","id":"f2a83b10-1558-4b06-a31f-42f411733608","artist-credit":[{"artist":{"name":"Seaside Rebels","id":"0ae11459-9245-44d0-b3c4-86424caa4fb6","disambiguation":"UK Oi / Punk","sort-name":"Seaside Rebels"},"name":"Seaside Rebels","joinphrase":""}],"length":null},"length":null,"artist-credit":[{"name":"Seaside Rebels","artist":{"sort-name":"Seaside Rebels","name":"Seaside Rebels","id":"0ae11459-9245-44d0-b3c4-86424caa4fb6","disambiguation":"UK Oi / Punk"},"joinphrase":""}]},{"length":null,"artist-credit":[{"joinphrase":"","name":"Seaside Rebels","artist":{"disambiguation":"UK Oi / Punk","name":"Seaside Rebels","id":"0ae11459-9245-44d0-b3c4-86424caa4fb6","sort-name":"Seaside Rebels"}}],"recording":{"title":"Overworked - Underfed - Underpaid","id":"f2940316-4f7a-4f72-9b08-cf19e7e643e6","disambiguation":"","video":false,"artist-credit":[{"joinphrase":"","artist":{"sort-name":"Seaside Rebels","id":"0ae11459-9245-44d0-b3c4-86424caa4fb6","name":"Seaside Rebels","disambiguation":"UK Oi / Punk"},"name":"Seaside Rebels"}],"length":null},"number":"B2","position":4,"id":"0149b575-0481-4be9-8142-bc2f24e9474f","title":"Overworked - Underfed - Underpaid"}],"format-id":"1d5a48e4-e94a-36dc-84e2-0c5f9f880aa7","track-count":4,"title":"","position":1,"track-offset":0}],"asin":null,"country":null,"id":"ee43c7ec-ec18-42d6-9115-de592a2f2aac","status-id":null,"release-events":[{"area":null,"date":"2017-05"}]}]}`)
		}))
		defer ts.Close()

		// Init the Mocks
		err := tests.InitCfg()
		require.Nil(err)

		ipfs.Cli = theIPFSMock

		cfg.Config.Set("MusicBrainz.URL", ts.URL)
		musicbrainz.Init()

		fTime := time.Now().Format("2006-01-02T15:04:05")
		err = Upload("7d90fe0a-a384-4289-9666-0bdee5d5957e", []byte("imagine this is the song content"))
		assert.EqualError(err, fmt.Sprintf("error adding the file /srv/marmota/library/Rude Pride _ Seaside Rebels/On Common Ground/Rude Pride - 1886 - %s to IPFS: this song is too good", fTime))
	})

	t.Run("should return an error if there's an error adding the file to the DB", func(t *testing.T) {
		fs.FS = afero.NewMemMapFs()

		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")

			fmt.Fprintf(w, `{"release-offset":0,"release-count":1,"releases":[{"barcode":null,"quality":"normal","date":"2015-04-28","title":"Duelo / Accidente","text-representation":{"script":"Latn","language":"spa"},"disambiguation":"","cover-art-archive":{"back":false,"artwork":true,"darkened":false,"front":true,"count":1},"artist-credit":[{"joinphrase":" / ","artist":{"sort-name":"Duelo","disambiguation":"Spanish hardcore","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo"},"name":"Duelo"},{"joinphrase":"","artist":{"sort-name":"Accidente","disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente"},"name":"Accidente"}],"status":"Official","packaging-id":null,"packaging":null,"status-id":"4e304316-386d-3409-af2e-78857eec5cfe","id":"4afebb2e-5d8e-422b-aab8-570bfa06c9df","country":"ES","release-events":[{"area":{"sort-name":"Spain","id":"471c46a7-afc5-31c4-923c-d0444f5053a4","name":"Spain","iso-3166-1-codes":["ES"],"disambiguation":""},"date":"2015-04-28"}],"asin":null,"media":[{"format-id":"1d5a48e4-e94a-36dc-84e2-0c5f9f880aa7","tracks":[{"number":"A1","position":1,"id":"7454b897-a596-47ff-b765-0c56ac22c839","title":"La utopía ya es real","length":171000,"artist-credit":[{"artist":{"sort-name":"Accidente","disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente"},"name":"Accidente","joinphrase":""}],"recording":{"disambiguation":"","video":false,"title":"La utopía ya es real","id":"8b5e3db2-7d48-43e0-82b3-2e2d8e942ed6","artist-credit":[{"joinphrase":"","name":"Accidente","artist":{"id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente","disambiguation":"","sort-name":"Accidente"}}],"length":171000}},{"recording":{"length":60000,"artist-credit":[{"joinphrase":"","name":"Accidente","artist":{"sort-name":"Accidente","disambiguation":"","name":"Accidente","id":"4461a9e7-2168-4453-a806-486c568190d5"}}],"disambiguation":"","video":false,"title":"Si te marchas ahora","id":"2f0cfe38-aac8-4f01-b025-4e796377e8cd"},"artist-credit":[{"joinphrase":"","artist":{"disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente","sort-name":"Accidente"},"name":"Accidente"}],"length":60000,"title":"Si te marchas ahora","number":"A2","position":2,"id":"807e0ec5-dc2d-427e-9cce-96cb56067882"},{"length":85000,"artist-credit":[{"artist":{"disambiguation":"","id":"4461a9e7-2168-4453-a806-486c568190d5","name":"Accidente","sort-name":"Accidente"},"name":"Accidente","joinphrase":""}],"recording":{"length":85000,"artist-credit":[{"name":"Accidente","artist":{"sort-name":"Accidente","name":"Accidente","id":"4461a9e7-2168-4453-a806-486c568190d5","disambiguation":""},"joinphrase":""}],"disambiguation":"","video":false,"title":"Querer la libertad","id":"2b502629-5fd0-418f-9212-e6e91d8c672d"},"position":3,"number":"A3","id":"2a12b326-fe4c-477a-a1c6-c77b645e4447","title":"Querer la libertad"},{"position":4,"id":"4669c59e-38ec-4708-b1a4-66ed84c2582c","number":"B1","title":"Comarcal 220","artist-credit":[{"artist":{"name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","disambiguation":"Spanish hardcore","sort-name":"Duelo"},"name":"Duelo","joinphrase":""}],"length":110000,"recording":{"artist-credit":[{"joinphrase":"","artist":{"sort-name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo","disambiguation":"Spanish hardcore"},"name":"Duelo"}],"length":110000,"title":"Comarcal 220","id":"ea46c5f3-b098-4eca-b20f-a558fea1306b","disambiguation":"","video":false}},{"artist-credit":[{"joinphrase":"","artist":{"sort-name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo","disambiguation":"Spanish hardcore"},"name":"Duelo"}],"length":63000,"recording":{"artist-credit":[{"name":"Duelo","artist":{"name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","disambiguation":"Spanish hardcore","sort-name":"Duelo"},"joinphrase":""}],"length":63000,"id":"af7fe28f-4570-4b25-9ce9-dfa30708dc98","title":"Buscar","video":false,"disambiguation":""},"number":"B2","position":5,"id":"32e076d3-aa90-4061-861a-0c77b0892ce8","title":"Buscar"},{"length":131000,"artist-credit":[{"name":"Duelo","artist":{"disambiguation":"Spanish hardcore","id":"473154da-c99d-41e8-9791-2300f388b0a8","name":"Duelo","sort-name":"Duelo"},"joinphrase":""}],"recording":{"artist-credit":[{"name":"Duelo","artist":{"disambiguation":"Spanish hardcore","name":"Duelo","id":"473154da-c99d-41e8-9791-2300f388b0a8","sort-name":"Duelo"},"joinphrase":""}],"length":131000,"id":"deee0375-f16f-4460-8fe7-e24a937d5bf5","title":"Madrid 2011","video":false,"disambiguation":""},"number":"B3","position":6,"id":"1fa36d6b-ffc1-4ab4-ab96-4c4cd8297791","title":"Madrid 2011"}],"format":"7\" Vinyl","position":1,"track-count":6,"title":"","track-offset":0}]}]}`)
		}))
		defer ts.Close()

		// Init the Mocks
		err := tests.InitCfg()
		require.Nil(err)

		err = tests.InitDB()
		require.Nil(err)

		ipfs.Cli = theIPFSMock

		cfg.Config.Set("MusicBrainz.URL", ts.URL)
		musicbrainz.Init()

		file := &models.File{
			ID:        cid,
			MBID:      trackID,
			ServerURL: cfg.Config.GetString("marmota.URL"),
			Bitrate:   0,
		}

		assert.Nil(db.DB.Create(file).Error)

		err = Upload(trackID, []byte("imagine this is the song content"))
		assert.EqualError(err, fmt.Sprintf("error adding %s to the DB: UNIQUE constraint failed: files.id", trackID))
	})
}
