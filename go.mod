module gitlab.com/nefix/marmota

require (
	cloud.google.com/go v0.36.0 // indirect
	github.com/btcsuite/btcd v0.0.0-20190213025234-306aecffea32 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20190204142019-df6d76eb9289 // indirect
	github.com/fsnotify/fsnotify v1.4.7
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/ipfs/go-ipfs-api v1.3.5
	github.com/ipfs/go-ipfs-cmdkit v1.1.3 // indirect
	github.com/jinzhu/gorm v1.9.2
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/lib/pq v1.0.0 // indirect
	github.com/libp2p/go-flow-metrics v0.0.0-20180906182756-7e5a55af4853 // indirect
	github.com/libp2p/go-libp2p-crypto v0.0.0-20190218135128-e333f2201582 // indirect
	github.com/libp2p/go-libp2p-metrics v0.0.0-20190218143726-eb0033e81c5e // indirect
	github.com/libp2p/go-libp2p-peer v0.0.0-20190218135252-9a193a66da54 // indirect
	github.com/libp2p/go-libp2p-protocol v0.0.0-20171212212132-b29f3d97e3a2 // indirect
	github.com/libp2p/go-libp2p-pubsub v0.0.0-20190221001050-30f6484345a6 // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/michiwend/gomusicbrainz v0.0.0-20181012083520-6c07e13dd396
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/multiformats/go-multiaddr v0.0.1 // indirect
	github.com/multiformats/go-multiaddr-dns v0.0.0-20181204224821-b3d6340f0777 // indirect
	github.com/multiformats/go-multiaddr-net v0.0.0-20190225134306-c8d587e921c0 // indirect
	github.com/spf13/afero v1.2.1
	github.com/spf13/jwalterweatherman v1.0.0
	github.com/spf13/viper v1.3.1
	github.com/stretchr/testify v1.3.0
	github.com/whyrusleeping/tar-utils v0.0.0-20180509141711-8c6c8ba81d5c // indirect
)
